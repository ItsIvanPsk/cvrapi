﻿using ItsyDev.DistributedServices.Models.Register;

namespace ItsyDev.DistributedServices.Models.Material;

public class ComponentDto
{
    public int ComponentId { get; set; }
    public string Name { get; set; } = string.Empty;
    public int MaterialId { get; set; }
    public MaterialDto Material { get; set; } = new();
    public ComponentStatus ComponentStatus { get; set; }
    public List<ProcedureDto> Procedures { get; set; } = new();
}