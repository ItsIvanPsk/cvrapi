﻿namespace ItsyDev.DistributedServices.Models.Material;

public class MaterialDto
{
    public int MaterialId { get; set; }
    public int VRType { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;

    public List<ComponentDto> Components { get; set; } = new();
}