﻿using ItsyDev.DistributedServices.Models.Others;

namespace ItsyDev.DistributedServices.Models.Material.List;

public class MaterialListDto
{
    public string Name { get; set; } = string.Empty;
    public List<ImageDto> Images { get; set; } = new();
}