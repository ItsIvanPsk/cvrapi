﻿using ItsyDev.DistributedServices.Models.Material;

namespace ItsyDev.DistributedServices.Models.Crate;

public class CrateDto
{
    public string CrateId { get; set; } = string.Empty;
    public List<ComponentDto> Components { get; set; }
}