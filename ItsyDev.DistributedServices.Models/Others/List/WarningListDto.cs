﻿namespace ItsyDev.DistributedServices.Models.Others.List;

public class WarningListDto
{
    public string Description { get; set; } = string.Empty;
}