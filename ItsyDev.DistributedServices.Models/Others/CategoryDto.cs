﻿namespace ItsyDev.DistributedServices.Models.Others;

public class CategoryDto
{
    public int CategoryId { get; set; }
    public string Name { get; set; } = string.Empty;
}