﻿namespace ItsyDev.DistributedServices.Models.Others;

public class ImageDto
{
    public int ImageId { get; set; }
    public string Src { get; set; } = string.Empty;
}