﻿namespace ItsyDev.DistributedServices.Models.Experience;

public enum Availability
{
    CLUB_VR,
    EXTERNAL
}