﻿using ItsyDev.DistributedServices.Models.Material;
using ItsyDev.DistributedServices.Models.Others;

namespace ItsyDev.DistributedServices.Models.Experience;

public class ExperienceDto
{
    public int ExperienceId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Rating { get; set; }
    public Availability Availability { get; set; } = Availability.CLUB_VR;
    public List<CategoryDto> Categories { get; set; } = new();
    public List<ImageDto> Images { get; set; } = new();
    public List<MaterialDto> Headsets { get; set; } = new();
}