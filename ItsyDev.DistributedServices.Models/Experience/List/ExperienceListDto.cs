﻿using ItsyDev.DistributedServices.Models.Material.List;
using ItsyDev.DistributedServices.Models.Others.List;

namespace ItsyDev.DistributedServices.Models.Experience.List;

public class ExperienceListDto
{
    public int ExperienceId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public Availability Availability { get; set; } = Availability.CLUB_VR;
    public List<CategoryListDto> Categories { get; set; } = new();
    public List<ImageListDto> Images { get; set; } = new();
    public List<MaterialListDto> Headsets { get; set; } = new();
}