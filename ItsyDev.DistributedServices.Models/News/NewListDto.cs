﻿namespace ItsyDev.DistributedServices.Models.News;

public class NewListDto
{
    public int NewId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public DateTime PublishDate { get; set; }
}