﻿namespace ItsyDev.DistributedServices.Models.Users;

public enum UserRole
{
    Miembro,
    Secretario,
    Tesorero,
    Presidente,
    Profesor
}