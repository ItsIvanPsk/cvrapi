﻿using ItsyDev.DistributedServices.Models.Material;

namespace ItsyDev.DistributedServices.Models.Register;

public class ProcedureDto
{
    public int ProcedureId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Src { get; set; } = string.Empty;
    public int ComponentId { get; set; }
    public int MaterialId { get; set; }
    public ComponentDto Component { get; set; } = new();
    public MaterialDto Material { get; set; } = new();
}