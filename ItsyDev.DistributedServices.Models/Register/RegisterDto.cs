﻿using ItsyDev.DistributedServices.Models.Material;
using ItsyDev.DistributedServices.Models.Users;

namespace ItsyDev.DistributedServices.Models.Register;

public class RegisterDto
{
    public int RegisterId { get; set; }
    public int MaterialId { get; set; }
    public MaterialDto Material { get; set; } = new();
    public DateTime RegisterDate { get; set; }
    public string Description { get; set; } = string.Empty;
    public int RegisterStatus { get; set; }
    public UserBe User { get; set; } = new();
}