﻿namespace ItsyDev.DistributedServices.Models.Register.Creation;

public class RegisterInvalidateRequestDto
{
    public int UserId { get; set; }
    public int RegisterId { get; set; }
    public string Reason { get; set; }
}