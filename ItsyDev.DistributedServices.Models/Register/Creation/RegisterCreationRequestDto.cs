﻿namespace ItsyDev.DistributedServices.Models.Register.Creation;

public class RegisterCreationRequestDto
{
    public int MaterialId { get; set; }
    public DateTime RegisterDate { get; set; }
    public int UserId { get; set; }
    public string Description { get; set; } = string.Empty;
    public int RegisterStatus { get; set; }
}