﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["ItsyDev.DistributedServices.WebApi/ItsyDev.DistributedServices.WebApi.csproj", "ItsyDev.DistributedServices.WebApi/"]
COPY ["ItsyDev.ApplicationServices.Services/ItsyDev.ApplicationServices.Services.csproj", "ItsyDev.ApplicationServices.Services/"]
COPY ["ItsyDev.DistributedServices.Models/ItsyDev.DistributedServices.Models.csproj", "ItsyDev.DistributedServices.Models/"]
COPY ["ItsyDev.DomainServices.Domain/ItsyDev.DomainServices.Domain.csproj", "ItsyDev.DomainServices.Domain/"]
COPY ["ItsyDev.DomainServices.Models/ItsyDev.DomainServices.Models.csproj", "ItsyDev.DomainServices.Models/"]
COPY ["ItsyDev.InfrastructureServices.Models/ItsyDev.InfrastructureServices.Models.csproj", "ItsyDev.InfrastructureServices.Models/"]
COPY ["ItsyDev.DomainServices.RepositoryContracts/ItsyDev.DomainServices.RepositoryContracts.csproj", "ItsyDev.DomainServices.RepositoryContracts/"]
COPY ["ItsyDev.CrossCutting.Models/ItsyDev.CrossCutting.Models.csproj", "ItsyDev.CrossCutting.Models/"]
COPY ["ItsyDev.InfrastructureServices.Repository/ItsyDev.InfrastructureServices.Repository.csproj", "ItsyDev.InfrastructureServices.Repository/"]
COPY ["ItsyDev.Infrastructure.Persistance/ItsyDev.Infrastructure.Persistance.csproj", "ItsyDev.Infrastructure.Persistance/"]
RUN dotnet restore "ItsyDev.DistributedServices.WebApi/ItsyDev.DistributedServices.WebApi.csproj"
COPY . .
WORKDIR "/src/ItsyDev.DistributedServices.WebApi"
RUN dotnet build "ItsyDev.DistributedServices.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ItsyDev.DistributedServices.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ItsyDev.DistributedServices.WebApi.dll"]
