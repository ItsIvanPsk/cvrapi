using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class NewsController : ControllerBase, INewsController
{
    private readonly INewsService _service;

    public NewsController(INewsService service)
    {
        _service = service;
    }

    [HttpGet("news-detail")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("news-detail")]
    public async Task<IActionResult> GetNewsDetail(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(1)] [FromQuery] int newId
    )
    {
        var result = await _service.GetNewsDetail(newId);
        return Ok(result);
    }

    [HttpGet("news-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("news-list")]
    public async Task<IActionResult> GetNewsList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        var result = await _service.GetNewsList();
        return Ok(result);
    }
}