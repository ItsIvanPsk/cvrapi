using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class MaterialController : ControllerBase, IMaterialController
{
    private readonly IMaterialService _service;

    public MaterialController(IMaterialService service)
    {
        _service = service;
    }

    [HttpGet("material-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("material-list")]
    public async Task<IActionResult> GetMaterialList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        var result = await _service.GetMaterialList();
        return Ok(result);
    }

    [HttpGet("headset-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("headset-list")]
    public async Task<IActionResult> GetHeadsetList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        var result = await _service.GetHeadsetList();
        return Ok(result);
    }

    [HttpPost("headset-detail")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("headset-detail")]
    public async Task<IActionResult> GetHeadsetDetail(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(1)] [FromBody] int headsetId
    )
    {
        var result = await _service.GetHeadsetDetail(headsetId);
        return Ok(result);
    }
}