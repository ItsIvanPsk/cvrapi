using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class UsersController : ControllerBase, IUsersController
{
    private readonly ILogger<ExperiencesController> _logger;
    private readonly IExperienceService _service;

    public UsersController(IExperienceService service, ILogger<ExperiencesController> logger)
    {
        _service = service;
        _logger = logger;
    }

    [HttpGet("users-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("users-list")]
    public async Task<IActionResult> GetUsersList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        throw new NotImplementedException();
    }
}