using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class ExperiencesController : ControllerBase, IExperiencesController
{
    private readonly IExperienceService _service;

    public ExperiencesController(IExperienceService service)
    {
        _service = service;
    }

    [HttpGet("experience-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("experience-list")]
    public async Task<IActionResult> GetExperienceList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        var result = await _service.GetExperienceList();
        return Ok(result);
    }


    [HttpGet("experience-detail")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("experience-detail")]
    public async Task<IActionResult> GetExperienceDetail(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(1)] [FromRoute] int experienceId
    )
    {
        var result = await _service.GetExperienceDetail(experienceId);
        return Ok(result);
    }
}