using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class CratesController : ControllerBase, ICratesController
{
    private readonly ICratesService _service;

    public CratesController(ICratesService service)
    {
        _service = service;
    }

    [HttpPost("crate-content")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("GetAccountConfiguration")]
    public async Task<IActionResult> GetCrateContent(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue("CVR_01")] [FromBody] int crateId
    )
    {
        var result = await _service.GetCrateContent(crateId);
        return Ok(result);
    }
}