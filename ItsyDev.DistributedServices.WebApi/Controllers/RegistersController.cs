using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.Register.Creation;
using ItsyDev.DistributedServices.WebApi.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace ItsyDev.DistributedServices.WebApi.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class RegistersController : ControllerBase, IRegistersController
{
    private readonly IRegistersService _service;

    public RegistersController(IRegistersService service)
    {
        _service = service;
    }

    [HttpGet("register-list")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("register-list")]
    public async Task<IActionResult> GetRegistersList(
        [DefaultValue(1)] [FromRoute] string version
    )
    {
        var result = await _service.GetRegistersList();
        return Ok(result);
    }

    [HttpGet("register-data")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("register-data")]
    public async Task<IActionResult> GetRegisterData(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(1)] [FromQuery] int registerId
    )
    {
        var result = await _service.GetRegisterData(registerId);
        return Ok(result);
    }

    [HttpPost("register-creation")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("register-creation")]
    public async Task<IActionResult> CreateRegister(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(
            "{\n    \"MaterialId\": 1,\n    \"RegisterDate\": \"2024-01-18T12:34:56\",\n    \"UserId\": 1,\n    \"Description\": \"Este es un ejemplo de descripción\",\n    \"RegisterStatus\": 1\n}")]
        [FromBody]
        RegisterCreationRequestDto request
    )
    {
        var result = await _service.CreateRegister(request);
        return Ok(result);
    }

    [HttpPost("register-invalidation")]
    [SwaggerResponse(StatusCodes.Status200OK, "Success")]
    [SwaggerOperation("register-invalidation")]
    public async Task<IActionResult> InvalidateRegister(
        [DefaultValue(1)] [FromRoute] string version,
        [DefaultValue(
            "{\n    \"UserId\": 5,\n    \"RegisterId\": 1,\n    \"Reason\": \"Esta es la razón de la invalidación.\"\n}")]
        [FromBody]
        RegisterInvalidateRequestDto request
    )
    {
        var result = await _service.InvalidateRegister(request);
        return Ok(result);
    }
}