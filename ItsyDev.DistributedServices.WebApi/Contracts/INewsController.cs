﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface INewsController
{
    Task<IActionResult> GetNewsList(
        [FromRoute] string version
    );

    Task<IActionResult> GetNewsDetail(
        [FromRoute] string version,
        [FromQuery] int newId
    );
}