﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface IMaterialController
{
    Task<IActionResult> GetMaterialList(
        [FromRoute] string version
    );

    Task<IActionResult> GetHeadsetList(
        [FromRoute] string version
    );

    Task<IActionResult> GetHeadsetDetail(
        [FromRoute] string version,
        [FromQuery] int headsetId
    );
}