﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface IUsersController
{
    Task<IActionResult> GetUsersList(
        [FromRoute] string version);
}