﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface IExperiencesController
{
    Task<IActionResult> GetExperienceList(
        [FromRoute] string version
    );

    Task<IActionResult> GetExperienceDetail(
        [FromRoute] string version,
        [FromQuery] int experienceId
    );
}