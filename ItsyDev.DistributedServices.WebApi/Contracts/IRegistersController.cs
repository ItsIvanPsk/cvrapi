﻿using ItsyDev.DistributedServices.Models.Register.Creation;
using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface IRegistersController
{
    Task<IActionResult> GetRegistersList([FromRoute] string version);
    Task<IActionResult> GetRegisterData([FromRoute] string version, [FromQuery] int registerId);
    Task<IActionResult> CreateRegister([FromRoute] string version, [FromBody] RegisterCreationRequestDto request);
    Task<IActionResult> InvalidateRegister([FromRoute] string version, [FromBody] RegisterInvalidateRequestDto request);
}