﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.DistributedServices.WebApi.Contracts;

public interface ICratesController
{
    Task<IActionResult> GetCrateContent(
        [FromRoute] string version,
        [FromBody] int crateId
    );
}