using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.ApplicationServices.Services.Implementations;
using ItsyDev.CrossCutting.Models;
using ItsyDev.DistributedServices.WebApi.Contracts;
using ItsyDev.DistributedServices.WebApi.Controllers;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Domain.Implementations;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddScoped<IExperiencesController, ExperiencesController>();
builder.Services.AddScoped<IExperienceService, ExperienceService>();
builder.Services.AddScoped<IExperienceDomain, ExperienceDomain>();
builder.Services.AddScoped<IExperienceRepository, ExperienceRepository>();

builder.Services.AddScoped<IMaterialController, MaterialController>();
builder.Services.AddScoped<IMaterialService, MaterialService>();
builder.Services.AddScoped<IMaterialDomain, MaterialDomain>();
builder.Services.AddScoped<IMaterialRepository, MaterialRepository>();

builder.Services.AddScoped<ICratesController, CratesController>();
builder.Services.AddScoped<ICratesService, CratesService>();
builder.Services.AddScoped<ICratesDomain, CratesDomain>();
builder.Services.AddScoped<ICratesRepository, CratesRepository>();

builder.Services.AddScoped<INewsController, NewsController>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<INewsDomain, NewsDomain>();
builder.Services.AddScoped<INewsRepository, NewsRepository>();

builder.Services.AddScoped<IRegistersController, RegistersController>();
builder.Services.AddScoped<IRegistersService, RegistersService>();
builder.Services.AddScoped<IRegistersDomain, RegistersDomain>();
builder.Services.AddScoped<IRegistersRepository, RegistersRepository>();

builder.Services.AddScoped<IUsersController, UsersController>();
builder.Services.AddScoped<IUsersService, UsersService>();
builder.Services.AddScoped<IUsersDomain, UsersDomain>();
builder.Services.AddScoped<IUsersRepository, UsersRepository>();

var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .Build();

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseMySql(configuration.GetConnectionString("Development"),
        new MySqlServerVersion(new Version(8, 0, 26)));
});


builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ReportApiVersions = true;
});

var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
var mapper = mappingConfig.CreateMapper();
builder.Services.AddSingleton(mapper);

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();