﻿using ItsyDev.DistributedServices.Models.Material;
using ItsyDev.DistributedServices.Models.Material.List;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface IMaterialService
{
    Task<List<MaterialListDto>> GetMaterialList();
    Task<List<MaterialListDto>> GetHeadsetList();
    Task<MaterialDto> GetHeadsetDetail(int headsetId);
}