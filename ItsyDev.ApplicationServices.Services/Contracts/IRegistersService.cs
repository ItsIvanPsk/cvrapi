﻿using ItsyDev.DistributedServices.Models.Register;
using ItsyDev.DistributedServices.Models.Register.Creation;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface IRegistersService
{
    Task<List<RegisterDto>> GetRegistersList();
    Task<RegisterDto> GetRegisterData(int registerId);
    Task<bool> CreateRegister(RegisterCreationRequestDto request);
    Task<bool> InvalidateRegister(RegisterInvalidateRequestDto request);
}