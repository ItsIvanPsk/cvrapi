﻿using ItsyDev.DistributedServices.Models.Crate;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface ICratesService
{
    Task<CrateDto> GetCrateContent(int crateId);
}