﻿using ItsyDev.DistributedServices.Models.News;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface INewsService
{
    Task<NewDto> GetNewsDetail(int newId);
    Task<List<NewListDto>> GetNewsList();
}