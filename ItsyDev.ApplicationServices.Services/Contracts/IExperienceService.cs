﻿using ItsyDev.DistributedServices.Models.Experience;
using ItsyDev.DistributedServices.Models.Experience.List;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface IExperienceService
{
    Task<List<ExperienceListDto>> GetExperienceList();
    Task<ExperienceDto> GetExperienceDetail(int experienceId);
}