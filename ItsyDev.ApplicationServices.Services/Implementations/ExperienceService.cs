﻿using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.Experience;
using ItsyDev.DistributedServices.Models.Experience.List;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class ExperienceService : IExperienceService
{
    private readonly IExperienceDomain _domain;
    private readonly IMapper _mapper;

    public ExperienceService(IExperienceDomain domain, IMapper mapper)
    {
        _domain = domain;
        _mapper = mapper;
    }

    public async Task<List<ExperienceListDto>> GetExperienceList()
    {
        var result = await _domain.GetExperienceList();
        return _mapper.Map<List<ExperienceListDto>>(result);
    }

    public async Task<ExperienceDto> GetExperienceDetail(int experienceId)
    {
        var result = await _domain.GetExperienceDetail(experienceId);
        return _mapper.Map<ExperienceDto>(result);
    }
}