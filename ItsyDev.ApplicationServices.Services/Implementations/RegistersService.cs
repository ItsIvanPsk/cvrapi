﻿using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.Register;
using ItsyDev.DistributedServices.Models.Register.Creation;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Register.Creation;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class RegistersService : IRegistersService
{
    private readonly IRegistersDomain _domain;
    private readonly IMapper _mapper;

    public RegistersService(IRegistersDomain domain, IMapper mapper)
    {
        _domain = domain;
        _mapper = mapper;
    }

    public async Task<List<RegisterDto>> GetRegistersList()
    {
        var result = await _domain.GetRegistersList();
        return _mapper.Map<List<RegisterDto>>(result);
    }

    public async Task<RegisterDto> GetRegisterData(int registerId)
    {
        var result = await _domain.GetRegisterData(registerId);
        return _mapper.Map<RegisterDto>(result);
    }

    public async Task<bool> CreateRegister(RegisterCreationRequestDto request)
    {
        var result = await _domain.CreateRegister(_mapper.Map<RegisterCreationRequestBe>(request));
        return result;
    }

    public async Task<bool> InvalidateRegister(RegisterInvalidateRequestDto request)
    {
        var result = await _domain.InvalidateRegister(_mapper.Map<RegisterInvalidateRequestBe>(request));
        return result;
    }
}