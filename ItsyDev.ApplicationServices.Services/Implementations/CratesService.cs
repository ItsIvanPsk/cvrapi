﻿using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.Crate;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class CratesService : ICratesService
{
    private readonly ICratesDomain _domain;
    private readonly IMapper _mapper;

    public CratesService(ICratesDomain domain, IMapper mapper)
    {
        _domain = domain;
        _mapper = mapper;
    }

    public async Task<CrateDto> GetCrateContent(int crateId)
    {
        var result = await _domain.GetCrateContent(crateId);
        return _mapper.Map<CrateDto>(result);
    }
}