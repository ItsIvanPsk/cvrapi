﻿using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.Material;
using ItsyDev.DistributedServices.Models.Material.List;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class MaterialService : IMaterialService
{
    private readonly IMaterialDomain _domain;
    private readonly IMapper _mapper;

    public MaterialService(IMaterialDomain domain, IMapper mapper)
    {
        _domain = domain;
        _mapper = mapper;
    }

    public async Task<List<MaterialListDto>> GetMaterialList()
    {
        var result = await _domain.GetMaterialList();
        return _mapper.Map<List<MaterialListDto>>(result);
    }

    public async Task<List<MaterialListDto>> GetHeadsetList()
    {
        var result = await _domain.GetHeadsetList();
        return _mapper.Map<List<MaterialListDto>>(result);
    }

    public async Task<MaterialDto> GetHeadsetDetail(int headsetId)
    {
        var result = await _domain.GetHeadsetDetail();
        return _mapper.Map<MaterialDto>(result);
    }
}