﻿using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DistributedServices.Models.News;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class NewsService : INewsService
{
    private readonly INewsDomain _domain;
    private readonly IMapper _mapper;

    public NewsService(INewsDomain domain, IMapper mapper)
    {
        _domain = domain;
        _mapper = mapper;
    }

    public async Task<NewDto> GetNewsDetail(int newId)
    {
        var result = await _domain.GetNewsDetail(newId);
        return _mapper.Map<NewDto>(result);
    }

    public async Task<List<NewListDto>> GetNewsList()
    {
        var result = await _domain.GetNewsList();
        return _mapper.Map<List<NewListDto>>(result);
    }
}