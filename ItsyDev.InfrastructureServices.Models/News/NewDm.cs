﻿namespace ItsyDev.InfrastructureServices.Models.News;

public class NewDm
{
    public int NewId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Subtitle { get; set; } = string.Empty;
    public string Body { get; set; } = string.Empty;
    public string Src { get; set; } = string.Empty;
    public DateTime PublishDate { get; set; }
}