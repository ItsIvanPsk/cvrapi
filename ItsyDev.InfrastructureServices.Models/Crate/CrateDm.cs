﻿namespace ItsyDev.InfrastructureServices.Models.Crate;

public class CrateDm
{
    public int CrateId { get; set; }
    public ICollection<CrateComponentsDm> CrateComponents { get; set; }
}