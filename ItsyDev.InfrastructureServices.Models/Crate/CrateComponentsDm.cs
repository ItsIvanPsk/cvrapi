﻿using ItsyDev.InfrastructureServices.Models.Material;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItsyDev.InfrastructureServices.Models.Crate;

public class CrateComponentsDm
{
    [ForeignKey("Crate")] public int CrateId { get; set; }
    public CrateDm Crate { get; set; }

    [ForeignKey("Component")] public int ComponentId { get; set; }
    public ComponentDm Component { get; set; }
}