﻿namespace ItsyDev.CrossCutting.Models;

public enum UserRole
{
    Miembro,
    Secretario,
    Tesorero,
    Presidente,
    Profesor
}