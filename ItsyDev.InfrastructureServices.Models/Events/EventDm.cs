﻿namespace ItsyDev.InfrastructureServices.Models.Events;

public class EventDm
{
    public int EventId { get; set; }
    public string Name { get; set; } = string.Empty;
    public DateTime Date { get; set; }
    public string QR { get; set; } = string.Empty;
}