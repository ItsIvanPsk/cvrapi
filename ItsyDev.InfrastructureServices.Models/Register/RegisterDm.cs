﻿using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.Users;

namespace ItsyDev.InfrastructureServices.Models.Register;

public class RegisterDm
{
    public int RegisterId { get; set; }
    public int MaterialId { get; set; }
    public MaterialDm Material { get; set; } = new();
    public DateTime RegisterDate { get; set; }
    public string Description { get; set; } = string.Empty;
    public int IsClosed { get; set; }
    public int RegisterStatus { get; set; }
    public int UserId { get; set; }
    public UserDm User { get; set; } = new();
    public ICollection<RegisterMaterialDm> RegisterMaterials { get; set; }
    public List<ProcedureDm> RegisterProcedures { get; set; }
}