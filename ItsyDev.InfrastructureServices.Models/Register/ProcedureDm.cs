﻿using ItsyDev.InfrastructureServices.Models.Material;

namespace ItsyDev.InfrastructureServices.Models.Register;

public class ProcedureDm
{
    public int ProcedureId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Src { get; set; } = string.Empty;
    public int ComponentId { get; set; }
    public int MaterialId { get; set; }
    public ComponentDm Component { get; set; } = new();
    public MaterialDm Material { get; set; } = new();
}