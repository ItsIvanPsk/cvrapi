﻿namespace ItsyDev.InfrastructureServices.Models.Register;

public class RegisterProcedureDm
{
    public int RegisterId { get; set; }
    public RegisterDm Register { get; set; }
    public int ProcedureId { get; set; }
    public ProcedureDm Procedure { get; set; }
}