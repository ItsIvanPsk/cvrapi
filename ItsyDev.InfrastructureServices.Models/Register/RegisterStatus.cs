﻿namespace ItsyDev.InfrastructureServices.Models.Register;

public enum RegisterStatus
{
    CORRECTO,
    CORRECTO_CON_INCIDENCIAS,
    FALLIDO
}