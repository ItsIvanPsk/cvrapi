﻿using ItsyDev.InfrastructureServices.Models.Material;

namespace ItsyDev.InfrastructureServices.Models.Register;

public class RegisterMaterialDm
{
    public int RegisterId { get; set; }
    public RegisterDm Register { get; set; }
    public int MaterialId { get; set; }
    public MaterialDm Material { get; set; }
}