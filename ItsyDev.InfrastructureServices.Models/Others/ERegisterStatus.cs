﻿namespace ItsyDev.InfrastructureServices.Models.Others;

public enum ERegisterStatus
{
    CORRECTO = 1,
    CORRECTO_CON_INCIDENCIAS = 2,
    FALLIDO = 3
}