﻿namespace ItsyDev.InfrastructureServices.Models.Others;

public enum EMaterialType
{
    HEADSET = 1,
    COMPLEMENTS = 2,
    OTHER = 3
}