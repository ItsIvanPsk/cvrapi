﻿using ItsyDev.InfrastructureServices.Models.Experience;

namespace ItsyDev.InfrastructureServices.Models.Others;

public class CategoryDm
{
    public int CategoryId { get; set; }
    public string Name { get; set; } = string.Empty;
    public IEnumerable<ExperienceCategoryDm>? ExperienceCategories { get; set; }
}