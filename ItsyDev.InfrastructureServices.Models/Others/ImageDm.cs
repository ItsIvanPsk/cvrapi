﻿using ItsyDev.InfrastructureServices.Models.Experience;

namespace ItsyDev.InfrastructureServices.Models.Others;

public class ImageDm
{
    public int ImageId { get; set; }
    public string Src { get; set; } = string.Empty;
    public IEnumerable<ExperienceImageDm>? ExperienceImages { get; set; }
}