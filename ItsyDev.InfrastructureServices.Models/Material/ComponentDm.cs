﻿using ItsyDev.CrossCutting.Models;
using ItsyDev.InfrastructureServices.Models.Crate;
using ItsyDev.InfrastructureServices.Models.Register;

namespace ItsyDev.InfrastructureServices.Models.Material;

public class ComponentDm
{
    public int ComponentId { get; set; }
    public string Name { get; set; } = string.Empty;
    public int MaterialId { get; set; }
    public MaterialDm Material { get; set; } = new();
    public ComponentStatus ComponentStatus { get; set; }
    public List<ProcedureDm> Procedures { get; set; } = new();
    public List<CrateDm> Crates { get; set; }
}