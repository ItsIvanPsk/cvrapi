﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItsyDev.InfrastructureServices.Models.Material;

public class MaterialComponentDm
{
    public MaterialDm Material { get; set; }


    public ComponentDm Component { get; set; }

    [Key]
    [Column(Order = 0)]
    [ForeignKey("Material")]
    public int MaterialId { get; set; }

    [Key]
    [Column(Order = 1)]
    [ForeignKey("Component")]
    public int ComponentId { get; set; }
}