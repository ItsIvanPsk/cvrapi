﻿using ItsyDev.InfrastructureServices.Models.Experience;
using ItsyDev.InfrastructureServices.Models.Register;

namespace ItsyDev.InfrastructureServices.Models.Material;

public class MaterialDm
{
    public int MaterialId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int VRType { get; set; }
    public int IsHeadset { get; set; }
    public IEnumerable<ExperienceMaterialDm>? Experiences { get; set; }
}