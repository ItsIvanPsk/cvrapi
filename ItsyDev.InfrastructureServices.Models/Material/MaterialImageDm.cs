﻿using ItsyDev.InfrastructureServices.Models.Others;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItsyDev.InfrastructureServices.Models.Material;

public class MaterialImageDm

{
    [ForeignKey("Material")] public int MaterialId { get; set; }
    public MaterialDm Material { get; set; }

    [ForeignKey("Image")] public int ImageId { get; set; }
    public ImageDm Image { get; set; }
}