﻿using ItsyDev.InfrastructureServices.Models.Material;

namespace ItsyDev.InfrastructureServices.Models.Experience;

public class ExperienceMaterialDm
{
    public int ExperienceId { get; set; }
    public ExperienceDm Experience { get; set; }
    public int MaterialId { get; set; }
    public MaterialDm Material { get; set; }
}