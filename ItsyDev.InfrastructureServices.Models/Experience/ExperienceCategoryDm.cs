﻿using ItsyDev.InfrastructureServices.Models.Others;

namespace ItsyDev.InfrastructureServices.Models.Experience;

public class ExperienceCategoryDm
{
    public int ExperienceId { get; set; }
    public ExperienceDm Experience { get; set; }
    public int CategoryId { get; set; }
    public CategoryDm Category { get; set; }
}