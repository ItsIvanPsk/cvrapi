﻿using ItsyDev.CrossCutting.Models;
using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.Others;

namespace ItsyDev.InfrastructureServices.Models.Experience;

public class ExperienceDm
{
    public int ExperienceId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Rating { get; set; }
    public Availability Availability { get; set; } = Availability.CLUB_VR;
    public IEnumerable<ExperienceCategoryDm>? ExperienceCategories { get; set; }
    public IEnumerable<ExperienceImageDm>? ExperienceImages { get; set; }
    public IEnumerable<ExperienceMaterialDm>? ExperienceHeadsets { get; set; }
}