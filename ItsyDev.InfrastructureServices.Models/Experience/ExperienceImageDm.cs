﻿using ItsyDev.InfrastructureServices.Models.Others;

namespace ItsyDev.InfrastructureServices.Models.Experience;

public class ExperienceImageDm
{
    public int ExperienceId { get; set; }
    public ExperienceDm Experience { get; set; }
    public int ImageId { get; set; }
    public ImageDm Image { get; set; }
}