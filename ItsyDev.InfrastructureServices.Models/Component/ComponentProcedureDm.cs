﻿using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.Register;

namespace ItsyDev.InfrastructureServices.Models.Component;

public class ComponentProcedureDm
{
    public int ComponentId { get; set; }
    public ComponentDm Component { get; set; }
    public int ProcedureId { get; set; }
    public ProcedureDm Procedure { get; set; }
}