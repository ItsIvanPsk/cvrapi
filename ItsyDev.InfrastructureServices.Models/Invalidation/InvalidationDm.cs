﻿using ItsyDev.InfrastructureServices.Models.Register;
using ItsyDev.InfrastructureServices.Models.Users;

namespace ItsyDev.InfrastructureServices.Models.Invalidation;

public class InvalidationDm
{
    public int InvalidationId { get; set; }
    public int UserId { get; set; }
    public UserDm User { get; set; }
    public int RegisterId { get; set; }
    public RegisterDm Register { get; set; }
    public string Reason { get; set; } = string.Empty;
    public DateTime InvalidationDate { get; set; }
}