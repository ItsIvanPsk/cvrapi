﻿using ItsyDev.DomainServices.Models.News;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface INewsDomain
{
    Task<List<NewListBe>> GetNewsList();
    Task<NewBe> GetNewsDetail(int newId);
}