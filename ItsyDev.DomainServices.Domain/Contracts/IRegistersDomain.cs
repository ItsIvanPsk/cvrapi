﻿using ItsyDev.DomainServices.Models.Register;
using ItsyDev.DomainServices.Models.Register.Creation;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface IRegistersDomain
{
    Task<List<RegisterBe>> GetRegistersList();
    Task<RegisterBe> GetRegisterData(int registerId);
    Task<bool> CreateRegister(RegisterCreationRequestBe request);
    Task<bool> InvalidateRegister(RegisterInvalidateRequestBe request);
}