﻿using ItsyDev.DomainServices.Models.Material;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface IMaterialDomain
{
    Task<List<MaterialListBe>> GetMaterialList();
    Task<List<MaterialListBe>> GetHeadsetList();
    Task<MaterialBe> GetHeadsetDetail();
}