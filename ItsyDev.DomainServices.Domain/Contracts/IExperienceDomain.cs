﻿using ItsyDev.DomainServices.Models.Experience;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface IExperienceDomain
{
    Task<List<ExperienceListBe>> GetExperienceList();
    Task<ExperienceBe> GetExperienceDetail(int experienceId);
}