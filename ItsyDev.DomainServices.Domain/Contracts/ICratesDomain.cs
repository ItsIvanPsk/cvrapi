﻿using ItsyDev.DomainServices.Models.Crate;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface ICratesDomain
{
    Task<CrateBe> GetCrateContent(int crateId);
}