﻿using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Crate;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class CratesDomain : ICratesDomain
{
    private readonly ICratesRepository _repository;
    private readonly IMapper _mapper;

    public CratesDomain(ICratesRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<CrateBe> GetCrateContent(int crateId)
    {
        var result = await _repository.GetCrateContent(crateId);
        return _mapper.Map<CrateBe>(result);
    }
}