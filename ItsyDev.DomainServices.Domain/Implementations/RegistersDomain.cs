﻿using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Register;
using ItsyDev.DomainServices.Models.Register.Creation;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class RegistersDomain : IRegistersDomain
{
    private readonly IMapper _mapper;
    private readonly IRegistersRepository _repository;

    public RegistersDomain(IRegistersRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<RegisterBe>> GetRegistersList()
    {
        var result = await _repository.GetRegistersList();
        return _mapper.Map<List<RegisterBe>>(result);
    }

    public async Task<RegisterBe> GetRegisterData(int registerId)
    {
        var result = await _repository.GetRegisterData(registerId);
        return _mapper.Map<RegisterBe>(result);
    }

    public async Task<bool> CreateRegister(RegisterCreationRequestBe request)
    {
        var result = await _repository.CreateRegister(request);
        return result;
    }

    public async Task<bool> InvalidateRegister(RegisterInvalidateRequestBe request)
    {
        var result = await _repository.InvalidateRegister(request);
        return result;
    }
}