﻿using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Experience;
using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.Models.Others.List;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.InfrastructureServices.Models.Material;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class ExperienceDomain : IExperienceDomain
{
    private readonly IMapper _mapper;
    private readonly IExperienceRepository _repository;

    public ExperienceDomain(IExperienceRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<ExperienceListBe>> GetExperienceList()
    {
        var result = await _repository.GetExperienceList();
        var experiences = result.Select(r => new ExperienceListBe
        {
            ExperienceId = r.ExperienceId,
            Name = r.Name,
            Description = r.Description,
            Categories = _mapper.Map<List<CategoryListBe>>(
                r.ExperienceCategories.Select(e => e.Category).ToList()
                ),
            Images = _mapper.Map<List<ImageListBe>>(
                r.ExperienceImages.Select(ei => ei.Image).ToList()
                ),
            Headsets = _mapper.Map<List<MaterialListBe>>(
                r.ExperienceHeadsets.Select(eh => eh.Material).ToList()
                )
        }).ToList();
        return experiences;
    }

    public async Task<ExperienceBe> GetExperienceDetail(int experienceId)
    {
        var result = await _repository.GetExperienceDetail(experienceId);
        return _mapper.Map<ExperienceBe>(result);
    }
}