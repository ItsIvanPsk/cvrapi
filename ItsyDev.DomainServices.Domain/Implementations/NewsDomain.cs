﻿using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.News;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class NewsDomain : INewsDomain
{
    private readonly IMapper _mapper;
    private readonly INewsRepository _repository;

    public NewsDomain(INewsRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<NewListBe>> GetNewsList()
    {
        var result = await _repository.GetNewsList();
        var news = result.Select(r => new NewListBe
        {
            NewId = r.NewId,
            Title = r.Title,
            Src = r.Src,
            PublishDate = r.PublishDate
        }).ToList();
        return news;
    }

    public async Task<NewBe> GetNewsDetail(int newId)
    {
        var result = await _repository.GetNewsDetail(newId);
        return _mapper.Map<NewBe>(result);
    }
}