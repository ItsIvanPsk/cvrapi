﻿using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class MaterialDomain : IMaterialDomain
{
    private readonly IMapper _mapper;
    private readonly IMaterialRepository _repository;

    public MaterialDomain(IMaterialRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }


    public async Task<List<MaterialListBe>> GetMaterialList()
    {
        var result = await _repository.GetMaterialList();
        return result.Select(r => new MaterialListBe
        {
            MaterialId = r.MaterialId,
            Name = r.Name
        }).ToList();
    }

    public async Task<List<MaterialListBe>> GetHeadsetList()
    {
        var result = await _repository.GetHeadsetList();
        return result.Select(r => new MaterialListBe
        {
            MaterialId = r.MaterialId,
            Name = r.Name
        }).ToList();
    }

    public async Task<MaterialBe> GetHeadsetDetail()
    {
        var result = await _repository.GetHeadsetList();
        var headset = result.Select(r => new MaterialBe
        {
            MaterialId = r.MaterialId,
            Name = r.Name,
            Description = r.Description
        }).FirstOrDefault();
        if (headset == null)
            throw new ArgumentNullException();

        return headset;
    }
}