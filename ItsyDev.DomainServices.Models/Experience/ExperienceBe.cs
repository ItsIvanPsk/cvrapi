﻿using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.Models.Others;

namespace ItsyDev.DomainServices.Models.Experience;

public class ExperienceBe
{
    public int ExperienceId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Rating { get; set; }
    public Availability Availability { get; set; } = Availability.CLUB_VR;
    public List<CategoryBe> Categories { get; set; } = new();
    public List<ImageBe> Images { get; set; } = new();
    public List<MaterialBe> Headsets { get; set; } = new();
}