﻿using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.Models.Others.List;

namespace ItsyDev.DomainServices.Models.Experience;

public class ExperienceListBe
{
    public int ExperienceId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public Availability Availability { get; set; } = Availability.CLUB_VR;
    public List<CategoryListBe> Categories { get; set; } = new();
    public List<ImageListBe> Images { get; set; } = new();
    public List<MaterialListBe> Headsets { get; set; } = new();
}