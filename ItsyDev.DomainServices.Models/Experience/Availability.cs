﻿namespace ItsyDev.DomainServices.Models.Experience;

public enum Availability
{
    CLUB_VR,
    EXTERNAL
}