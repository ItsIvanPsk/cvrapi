﻿namespace ItsyDev.DomainServices.Models.News;

public class NewBe
{
    public int NewId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Subtitle { get; set; } = string.Empty;
    public string Body { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public DateTime PublishDate { get; set; }
}