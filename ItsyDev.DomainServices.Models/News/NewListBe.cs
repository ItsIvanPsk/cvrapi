﻿namespace ItsyDev.DomainServices.Models.News;

public class NewListBe
{
    public int NewId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Src { get; set; } = string.Empty;
    public DateTime PublishDate { get; set; }
}