﻿using ItsyDev.DomainServices.Models.Material;

namespace ItsyDev.DomainServices.Models.Register;

public class ProcedureBe
{
    public int ProcedureId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Src { get; set; } = string.Empty;
    public int ComponentId { get; set; }
    public int MaterialId { get; set; }
    public ComponentBe Component { get; set; } = new();
    public MaterialBe Material { get; set; } = new();
}