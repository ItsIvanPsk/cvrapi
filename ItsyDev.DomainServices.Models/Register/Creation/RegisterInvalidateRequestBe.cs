﻿namespace ItsyDev.DomainServices.Models.Register.Creation;

public class RegisterInvalidateRequestBe
{
    public int RegisterId { get; set; }
    public string Reason { get; set; } = string.Empty;
    public int UserId { get; set; }
}