﻿using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.Models.Users;
using ItsyDev.InfrastructureServices.Models.Register;

namespace ItsyDev.DomainServices.Models.Register;

public class RegisterBe
{
    public int RegisterId { get; set; }
    public int MaterialId { get; set; }
    public MaterialBe Material { get; set; } = new();
    public DateTime RegisterDate { get; set; }
    public string Description { get; set; } = string.Empty;
    public RegisterStatus RegisterStatus { get; set; }
    public UserBe User { get; set; } = new();
}