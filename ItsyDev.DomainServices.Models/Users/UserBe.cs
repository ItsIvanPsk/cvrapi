﻿namespace ItsyDev.DomainServices.Models.Users;

public class UserBe
{
    public int UserId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Surname { get; set; } = string.Empty;
    public string Username { get; set; } = string.Empty;
    public string Pwd { get; set; } = string.Empty;
    public int Telf { get; set; }
    public string UserMail { get; set; } = string.Empty;
    public UserRole UserRole { get; set; }
    public bool RPerm { get; set; }
    public bool WPerm { get; set; }
    public bool TPerm { get; set; }
    public bool FPerm { get; set; }
    public DateTime JoinDate { get; set; }
}