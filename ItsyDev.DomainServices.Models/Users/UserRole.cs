﻿namespace ItsyDev.DomainServices.Models.Users;

public enum UserRole
{
    Miembro,
    Secretario,
    Tesorero,
    Presidente,
    Profesor
}