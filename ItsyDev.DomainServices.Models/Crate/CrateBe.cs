﻿using ItsyDev.DomainServices.Models.Material;

namespace ItsyDev.DomainServices.Models.Crate;

public class CrateBe
{
    public string CrateId { get; set; }
    public List<ComponentBe> Components { get; set; }
}