﻿using ItsyDev.DomainServices.Models.Others;

namespace ItsyDev.DomainServices.Models.Material;

public class MaterialListBe
{
    public int MaterialId { get; set; }
    public string Name { get; set; } = string.Empty;
    public List<ImageBe> Images { get; set; }
}