﻿namespace ItsyDev.DomainServices.Models.Material;

public class MaterialBe
{
    public int MaterialId { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int VRType { get; set; }

    public List<ComponentBe> Components { get; set; } = new();
}