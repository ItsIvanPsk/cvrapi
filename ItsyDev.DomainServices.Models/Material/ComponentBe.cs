﻿using ItsyDev.DomainServices.Models.Crate;
using ItsyDev.DomainServices.Models.Register;

namespace ItsyDev.DomainServices.Models.Material;

public class ComponentBe
{
    public int ComponentId { get; set; }
    public string Name { get; set; } = string.Empty;
    public int MaterialId { get; set; }
    public MaterialBe Material { get; set; } = new();
    public ComponentStatus ComponentStatus { get; set; }
    public List<ProcedureBe> Procedures { get; set; } = new();
    public List<CrateBe> Crates { get; set; }
}