﻿namespace ItsyDev.DomainServices.Models.Material;

public enum ComponentStatus
{
    BUEN_ESTADO,
    MAL_ESTADO,
    NO_ESTA
}