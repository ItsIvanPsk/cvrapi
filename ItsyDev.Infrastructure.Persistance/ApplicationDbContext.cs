﻿using ItsyDev.InfrastructureServices.Models.Crate;
using ItsyDev.InfrastructureServices.Models.Experience;
using ItsyDev.InfrastructureServices.Models.Invalidation;
using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.News;
using ItsyDev.InfrastructureServices.Models.Others;
using ItsyDev.InfrastructureServices.Models.Register;
using ItsyDev.InfrastructureServices.Models.Users;
using Microsoft.EntityFrameworkCore;

namespace ItsyDev.Infrastructure.Persistance;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<UserDm>? Users { get; set; }
    public DbSet<ExperienceDm>? Experiences { get; set; }
    public DbSet<ImageDm>? Images { get; set; }
    public DbSet<MaterialDm>? MaterialDm { get; set; }
    public DbSet<ExperienceImageDm>? ExperienceImages { get; set; }
    public DbSet<ExperienceCategoryDm>? ExperienceCategories { get; set; }
    public DbSet<ExperienceMaterialDm>? ExperienceMaterial { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder = UsersConfig(modelBuilder);
        modelBuilder = ExperienceConfig(modelBuilder);
        modelBuilder = CategoryConfig(modelBuilder);
        modelBuilder = ImageConfig(modelBuilder);
        modelBuilder = MaterialConfig(modelBuilder);
    }

    private static ModelBuilder ExperienceConfig(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ExperienceDm>().ToTable("Experience");
        modelBuilder.Entity<ExperienceDm>()
            .HasKey(e => e.ExperienceId);
        modelBuilder.Entity<ExperienceDm>()
            .Property(e => e.Name)
            .HasMaxLength(255);
        modelBuilder.Entity<ExperienceDm>()
            .Property(e => e.Description)
            .HasMaxLength(555);
        modelBuilder.Entity<ExperienceDm>()
            .Property(e => e.Rating);
        modelBuilder.Entity<ExperienceDm>()
            .Property(e => e.Availability)
            .HasConversion<string>()
            .HasMaxLength(50);
        modelBuilder.Entity<ExperienceImageDm>()
            .HasKey(ec => new { ec.ExperienceId, ec.ImageId });
        modelBuilder.Entity<ExperienceImageDm>()
            .HasOne(ec => ec.Experience)
            .WithMany(e => e.ExperienceImages)
            .HasForeignKey(ec => ec.ExperienceId);

        modelBuilder.Entity<ExperienceImageDm>()
            .HasOne(ec => ec.Image)
            .WithMany(c => c.ExperienceImages)
            .HasForeignKey(ec => ec.ImageId);

        modelBuilder.Entity<ExperienceCategoryDm>()
            .HasKey(ec => new { ec.ExperienceId, ec.CategoryId });
        modelBuilder.Entity<ExperienceCategoryDm>()
            .HasOne(ec => ec.Experience)
            .WithMany(e => e.ExperienceCategories)
            .HasForeignKey(ec => ec.ExperienceId);

        modelBuilder.Entity<ExperienceCategoryDm>()
            .HasOne(ec => ec.Category)
            .WithMany(c => c.ExperienceCategories)
            .HasForeignKey(ec => ec.CategoryId);

        modelBuilder.Entity<ExperienceMaterialDm>()
            .HasKey(ec => new { ec.ExperienceId, ec.MaterialId });
        modelBuilder.Entity<ExperienceMaterialDm>()
            .HasOne(ec => ec.Experience)
            .WithMany(e => e.ExperienceHeadsets)
            .HasForeignKey(ec => ec.ExperienceId);

        modelBuilder.Entity<ExperienceMaterialDm>()
            .HasOne(ec => ec.Material)
            .WithMany(c => c.Experiences)
            .HasForeignKey(ec => ec.MaterialId);


        return modelBuilder;
    }

    private static ModelBuilder UsersConfig(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserDm>().ToTable("Users");
        modelBuilder.Entity<UserDm>()
            .HasKey(u => u.UserId);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.Name)
            .HasMaxLength(255);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.Surname)
            .HasMaxLength(255);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.Username)
            .HasMaxLength(255);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.Pwd)
            .HasMaxLength(255);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.Telf);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.UserMail)
            .HasMaxLength(255);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.UserRole);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.RPerm);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.WPerm);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.TPerm);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.FPerm);
        modelBuilder.Entity<UserDm>()
            .Property(u => u.JoinDate);
        return modelBuilder;
    }

    private static ModelBuilder CategoryConfig(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<CategoryDm>().ToTable("Category");
        modelBuilder.Entity<CategoryDm>()
            .HasKey(c => c.CategoryId);
        modelBuilder.Entity<CategoryDm>()
            .Property(c => c.Name)
            .HasMaxLength(999);
        return modelBuilder;
    }

    private static ModelBuilder ImageConfig(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ImageDm>().ToTable("Images");
        modelBuilder.Entity<ImageDm>()
            .HasKey(i => i.ImageId);
        modelBuilder.Entity<ImageDm>()
            .Property(i => i.Src)
            .HasMaxLength(255);
        return modelBuilder;
    }

    private static ModelBuilder MaterialConfig(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MaterialDm>().ToTable("Material");
        modelBuilder.Entity<MaterialDm>()
            .HasKey(m => m.MaterialId);
        modelBuilder.Entity<MaterialDm>()
            .Property(m => m.Name)
            .HasMaxLength(255);
        modelBuilder.Entity<MaterialDm>()
            .Property(m => m.Description)
            .HasMaxLength(255);
        modelBuilder.Entity<MaterialDm>()
            .Property(m => m.IsHeadset);
        modelBuilder.Entity<MaterialDm>()
            .Property(m => m.VRType);
        return modelBuilder;
    }
}