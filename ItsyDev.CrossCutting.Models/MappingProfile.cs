﻿using AutoMapper;
using ItsyDev.DistributedServices.Models.Crate;
using ItsyDev.DistributedServices.Models.Experience;
using ItsyDev.DistributedServices.Models.Experience.List;
using ItsyDev.DistributedServices.Models.Material;
using ItsyDev.DistributedServices.Models.Material.List;
using ItsyDev.DistributedServices.Models.News;
using ItsyDev.DistributedServices.Models.Others;
using ItsyDev.DistributedServices.Models.Others.List;
using ItsyDev.DistributedServices.Models.Register.Creation;
using ItsyDev.DistributedServices.Models.Users;
using ItsyDev.DomainServices.Models.Crate;
using ItsyDev.DomainServices.Models.Experience;
using ItsyDev.DomainServices.Models.Material;
using ItsyDev.DomainServices.Models.News;
using ItsyDev.DomainServices.Models.Others;
using ItsyDev.DomainServices.Models.Others.List;
using ItsyDev.DomainServices.Models.Register.Creation;
using ItsyDev.InfrastructureServices.Models.Crate;
using ItsyDev.InfrastructureServices.Models.Experience;
using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.News;
using ItsyDev.InfrastructureServices.Models.Others;
using ItsyDev.InfrastructureServices.Models.Users;


namespace ItsyDev.CrossCutting.Models;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        ExperienceMap();
        ComponentMap();
        CrateMap();
        MaterialMap();
        NewsMap();
        RegisterMap();
        UserMap();
        CategoryMap();
        ImageMap();

        RegisterRequestsMap();
    }

    private void RegisterRequestsMap()
    {
        CreateMap<RegisterInvalidateRequestDto, RegisterInvalidateRequestBe>().ReverseMap();
        CreateMap<RegisterCreationRequestDto, RegisterCreationRequestBe>().ReverseMap();
    }

    private void ComponentMap()
    {
        CreateMap<ComponentDto, ComponentBe>().ReverseMap();
        CreateMap<ComponentBe, ComponentDm>().ReverseMap();
    }

    private void ImageMap()
    {
        CreateMap<ImageDto, ImageBe>().ReverseMap();
        CreateMap<ImageBe, ImageDm>().ReverseMap();
        CreateMap<ImageDto, ImageListDto>().ReverseMap();
        CreateMap<ImageBe, ImageListBe>().ReverseMap();
        CreateMap<ImageDm, ImageListBe>().ReverseMap();
        CreateMap<ImageListDto, ImageListBe>().ReverseMap();
    }

    private void CategoryMap()
    {
        CreateMap<CategoryDto, CategoryListDto>().ReverseMap();
        CreateMap<CategoryBe, CategoryListBe>().ReverseMap();
        CreateMap<CategoryDm, CategoryListBe>().ReverseMap();
        CreateMap<CategoryListDto, CategoryListBe>().ReverseMap();
        CreateMap<CategoryDto, CategoryBe>().ReverseMap();
        CreateMap<CategoryBe, CategoryDm>().ReverseMap();
    }

    private void UserMap()
    {
        CreateMap<UserBe, UserDm>().ReverseMap();
    }

    private void RegisterMap()
    {
    }

    private void NewsMap()
    {
        CreateMap<NewDto, NewBe>().ReverseMap();
        CreateMap<NewBe, NewDm>().ReverseMap();
        CreateMap<NewListDto, NewListBe>().ReverseMap();
    }

    private void MaterialMap()
    {
        CreateMap<MaterialDto, MaterialListDto>().ReverseMap();
        CreateMap<MaterialBe, MaterialListBe>().ReverseMap();
        CreateMap<MaterialDm, MaterialListBe>().ReverseMap();
        CreateMap<MaterialListDto, MaterialListBe>().ReverseMap();
        CreateMap<MaterialDto, MaterialBe>().ReverseMap();
        CreateMap<MaterialBe, MaterialDm>().ReverseMap();

        CreateMap<MaterialListDto, MaterialListBe>().ReverseMap();
        CreateMap<MaterialListBe, MaterialDm>().ReverseMap();
    }

    private void ExperienceMap()
    {
        CreateMap<ExperienceListDto, ExperienceListBe>().ReverseMap();
        CreateMap<ExperienceListBe, ExperienceDm>().ReverseMap();

        CreateMap<ExperienceDm, ExperienceBe>().ReverseMap();
        CreateMap<ExperienceBe, ExperienceDto>().ReverseMap();

        CreateMap<ExperienceCategoryDm, CategoryListBe>();
        CreateMap<ExperienceImageDm, ImageListBe>();
        CreateMap<ExperienceMaterialDm, MaterialListBe>();
    }

    private void CrateMap()
    {
        CreateMap<CrateDto, CrateBe>().ReverseMap();
        CreateMap<CrateBe, CrateDm>().ReverseMap();
    }
}