# API de Ejemplo

## Descripción

La API del club de realidad virtual proporciona acceso a los datos del club. Esta API se proporciona exclusivamente con
fines educativos y de demostración y no está destinada para su uso en ningún proyecto de producción.

## Requisitos previos

Asegúrate de tener los siguientes requisitos previos antes de comenzar:

- Docker instalado en el sistema

## Uso

Para comenzar a utilizar la API, sigue estos pasos:

1. Haz un pull de la imagen de la API:

   ```bash
   docker pull https://hub.docker.com/repository/docker/itsivanpsk/cvrapi-webapi
2. Haz un pull de la imagen de la base de datos:

   ```bash
   docker pull https://hub.docker.com/repository/docker/itsivanpsk/mysql
3. Ejecuta el docker-compose para poder obtener el compose con ambas imagenes.
4. Accede a la API en tu navegador o mediante una herramienta como Postman:
    ```bash
    http://localhost:[PORT]/api/

## Contribución

Este proyecto no acepta contribuciones en forma de código. Si deseas colaborar de alguna manera, contáctanos a través
de [tu dirección de correo electrónico].

## Licencia

El código fuente de esta API está protegido por la siguiente licencia:

Copyright (c) 2023 ItsyDev Studios

The source code of this API is provided exclusively for educational and demonstration purposes. No rights are granted to
use this code in production projects or for commercial purposes.

Any use, redistribution, or modification of this code for any purpose other than those mentioned above is strictly
prohibited.
