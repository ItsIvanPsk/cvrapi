﻿using AutoMapper;
using ItsyDev.DomainServices.Models.Register.Creation;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.Others;
using ItsyDev.InfrastructureServices.Models.Register;
using Microsoft.EntityFrameworkCore;

namespace ItsyDev.InfrastructureServices.Repository;

public class RegistersRepository : IRegistersRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public RegistersRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<bool> InvalidateRegister(RegisterInvalidateRequestBe request)
    {
        return false;
    }

    public async Task<bool> CreateRegister(RegisterCreationRequestBe request)
    {
        return false;
    }

    private async Task<bool> AddRegisterMaterial(RegisterDm register, MaterialDm material)
    {
        return false;
    }

    public async Task<RegisterDm> GetRegisterData(int registerId)
    {
        return null;
    }

    public async Task<List<RegisterDm>> GetRegistersList()
    {
        return null;
    }
}