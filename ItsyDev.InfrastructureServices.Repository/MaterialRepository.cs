﻿using AutoMapper;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Models.Material;
using ItsyDev.InfrastructureServices.Models.Others;
using Microsoft.EntityFrameworkCore;

namespace ItsyDev.InfrastructureServices.Repository;

public class MaterialRepository : IMaterialRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public MaterialRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<MaterialDm>> GetMaterialList()
    {
        return null;
    }

    public async Task<List<MaterialDm>> GetHeadsetList()
    {
        return null;
    }

    public async Task<MaterialDm> GetHeadsetDetail(int headsetId)
    {
        return null;
    }
}