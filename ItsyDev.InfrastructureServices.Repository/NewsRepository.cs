﻿using AutoMapper;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Models.News;
using Microsoft.EntityFrameworkCore;

namespace ItsyDev.InfrastructureServices.Repository;

public class NewsRepository : INewsRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public NewsRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<NewDm>> GetNewsList()
    {
        return null;
    }

    public async Task<NewDm> GetNewsDetail(int newId)
    {
        return null;
    }
}