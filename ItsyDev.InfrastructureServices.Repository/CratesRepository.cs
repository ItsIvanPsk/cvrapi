﻿using AutoMapper;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Models.Crate;
using Microsoft.EntityFrameworkCore;

namespace ItsyDev.InfrastructureServices.Repository;

public class CratesRepository : ICratesRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public CratesRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<CrateDm> GetCrateContent(int crateId)
    {
        return null;
    }
}