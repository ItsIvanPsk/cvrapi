﻿using AutoMapper;
using ItsyDev.DomainServices.RepositoryContracts.Contracts;
using ItsyDev.Infrastructure.Persistance;
using ItsyDev.InfrastructureServices.Models.Experience;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace ItsyDev.InfrastructureServices.Repository;

public class ExperienceRepository : IExperienceRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public ExperienceRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<ExperienceDm>> GetExperienceList()
    {
        var result = await _dbContext.Experiences
            .Include(ec => ec.ExperienceCategories)
                .ThenInclude(ec => ec.Category)
            .Include(ei => ei.ExperienceImages)
                .ThenInclude(ei => ei.Image)
            .Include(eh => eh.ExperienceHeadsets)
                .ThenInclude(eh => eh.Material)
            .ToListAsync();

        return result;
    }


    public async Task<ExperienceDm> GetExperienceDetail(int experienceId)
    {
        return null;
    }
}