﻿using ItsyDev.InfrastructureServices.Models.News;

namespace ItsyDev.DomainServices.RepositoryContracts.Contracts;

public interface INewsRepository
{
    Task<List<NewDm>> GetNewsList();
    Task<NewDm> GetNewsDetail(int newId);
}