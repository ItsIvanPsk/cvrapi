﻿using ItsyDev.InfrastructureServices.Models.Material;

namespace ItsyDev.DomainServices.RepositoryContracts.Contracts;

public interface IMaterialRepository
{
    Task<List<MaterialDm>> GetMaterialList();
    Task<List<MaterialDm>> GetHeadsetList();
    Task<MaterialDm> GetHeadsetDetail(int headsetId);
}