﻿using ItsyDev.InfrastructureServices.Models.Experience;

namespace ItsyDev.DomainServices.RepositoryContracts.Contracts;

public interface IExperienceRepository
{
    Task<List<ExperienceDm>> GetExperienceList();
    Task<ExperienceDm> GetExperienceDetail(int experienceId);
}