﻿using ItsyDev.InfrastructureServices.Models.Crate;

namespace ItsyDev.DomainServices.RepositoryContracts.Contracts;

public interface ICratesRepository
{
    Task<CrateDm> GetCrateContent(int crateId);
}