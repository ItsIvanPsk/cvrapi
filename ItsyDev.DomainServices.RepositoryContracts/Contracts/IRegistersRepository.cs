﻿using ItsyDev.DomainServices.Models.Register.Creation;
using ItsyDev.InfrastructureServices.Models.Register;

namespace ItsyDev.DomainServices.RepositoryContracts.Contracts;

public interface IRegistersRepository
{
    Task<bool> InvalidateRegister(RegisterInvalidateRequestBe request);
    Task<bool> CreateRegister(RegisterCreationRequestBe request);
    Task<RegisterDm> GetRegisterData(int registerId);
    Task<List<RegisterDm>> GetRegistersList();
}